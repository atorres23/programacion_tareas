#include <stdio.h>
/*Se debe compilar g++ -DLETRA=\'d\' mays.cpp -o mays
 * Se pone -DLETRA porque no hemos definido LETRA entonces para que podamos compilar con el make nos dara error por no definirlo pero de esta manera forzamos a que compile con -DLETRA*/

#define SALTO ('a' - 'A')
/*define LETRA 'c'*/

int main () {
    char letra = LETRA;
    /*Algo*/

/*Esta es la linea que vamos a introducir*/
/*Si la letra es mayuscula*/   
if (letra >= 'a' && letra <= 'z')  
    letra -= SALTO;
/*Si no*/
else 
    letra += SALTO;
    
    
        printf ("letra en mayusculas: %c\n", letra); 
    
    return 0;
}
