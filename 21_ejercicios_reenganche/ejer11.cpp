/*
 * =====================================================================================
 *
 *       Filename:  ejer11.cpp
 *
 *    Description: Preguntar al usuario por la temperatura y di usando un switch si es Hielo, Liquido o Vapor 
 *
 *        Version:  1.0
 *        Created:  14/06/20 18:00:00
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejadro Torres Guerra
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){
    int temperatura;
    int estado;

    printf("Introduce la temperatura a la que esta el agua:\n");
    scanf("%d",&temperatura);

    if (temperatura<0){
        estado = 1;
    }else{
        if (temperatura>=0 && temperatura<100){
            estado = 2;
        }else{
            if (temperatura>=100){
                estado = 3;
            }
        }
    }
    
    switch(estado){
        case 1:
            printf("El agua esta en estado de hielo\n", temperatura);
        break;
        case 2:
            printf("El agua esta en estado liquido\n", temperatura);
        break;
        case 3:
            printf("El agua esta en estado gaseoso\n", temperatura);
        break;
    }
    
    return EXIT_SUCCESS;    
}
