/*
 * =====================================================================================
 *
 *       Filename:  ejer12.cpp
 *
 *    Description: Escrube un for que leyendo del array de caracteres texto, escriba en otro el texto al reves. CUidado con \n que siempre va al final 
 *
 *        Version:  1.0
 *        Created:  14/06/20 18:26:32
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejandro Torres Guerra
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX 5

int main (int argc, char *argv[]){
    char texto[MAX]{'h','o','l','a','\n'};
    char texto_al_reves[MAX];

    for(int a=0;a<MAX;a++)
        texto_al_reves[a]=texto[MAX-1-a];

    printf("%s\n",texto_al_reves);

    return EXIT_SUCCESS;

}


