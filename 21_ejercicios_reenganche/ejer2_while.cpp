
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    
    int resultado, i=2;

    while (i<100){
        resultado += i;
        i += 3;
    }

    printf ("La suma de cada tercer entero es: %i\n", resultado);
  

    return EXIT_SUCCESS;
}
