#include <stdio.h>
#include <stdlib.h>
#include <cstring>

#define G 5

int main (int argc, char *argv[]) {

    int ch_letra = 0,
        ch_numero  = 0,
        suma;
    char texto[G] =  {
        'M',
        'n',
        'K',
        '1',
        'd',
    };

    for (int i=0; i<G; i++){
        if (texto[i] >= 65 && texto[i] <= 122){
            ch_letra ++;
        }else if (texto[i] >= 48 && texto[i] <= 57){
            ch_numero ++;
        }
    }

    suma = ch_letra + ch_numero;
    printf("Hay %i letra/s \n", ch_letra);
    printf("Hay %i numero/s \n", ch_numero);
    printf("En total hay %i caracteres \n", suma);


    return EXIT_SUCCESS;
}
