/*
 * =====================================================================================
 *
 *       Filename:  ejer13.cpp
 *
 *    Description: Array de origen y destino han de ser el mismo, solo hay un array y este tiene que quedar invertido. 
 *
 *        Version:  1.0
 *        Created:  15/06/20 10:40:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejandro Torres Guerra
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 32

int main (int argc, char *argv[]){
    char texto[MAX] = "Dabale arroz a la zorra el abad";

    int a = 0;
    int b;

printf("La cadena que se va a dar la vuelta es:%s\n",texto);

while(texto[a] != '\0'){
    a++;
}

for(b=a-1; b>=0; b--)
  printf("La cadena dada la vuelta es:%c\n",texto[b]);

    return EXIT_SUCCESS;
}    
