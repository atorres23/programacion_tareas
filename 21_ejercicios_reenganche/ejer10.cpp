/*
 * =====================================================================================
 *
 *       Filename:  ejer10.cpp
 *
 *    Description: Haz un switch que examine la variable de tipo caracter color cuyos valores pueden ser 'r','g','b' en mayusculas o minusculas. 
 *
 *        Version:  1.0
 *        Created:  14/06/20 17:44:41
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejandro Torres Guerra
 *   Organization:  
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h> //necesario para tolower

int main(int argc, char *argv[]){
  char color;
  char letra_minuscula;

  printf("Introduce la letra R(rojo) ,G(verde) ,B(azul) dependiendo del color que quiera elegir:\n");
  scanf("%c", &color);

  letra_minuscula=tolower(color); //pasa una letra de mayuscula a minuscula

  switch(letra_minuscula){
      case 'r':
          printf("El color que eliges es el rojo al haber introducido el caracter %c\n",letra_minuscula);
      break;
      case 'g':
          printf("El color que eliges es el verde al haber introducido el caracter %c\n",letra_minuscula);  
      break;
      case 'b':
          printf("El color que eliges es el azul al haber introducido el caracter %c\n",letra_minuscula);
      break;
      default:
          printf("No has elegido ningun color correcto\n");
      break;
  }

  return EXIT_SUCCESS;
}

