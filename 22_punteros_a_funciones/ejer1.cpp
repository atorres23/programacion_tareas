/*
 * =====================================================================================
 *
 *       Filename:  ejer1.cpp
 *
 *    Description: Dadas base y altura de un triangulo o rectangulo y teniendo dos funciones que calculan areas, imprimir el area de lo que quiera el usuario usando un puntero que a veces apunte a una funcion y a veces a otra. NO USAR SWITCH 
 *
 *        Version:  1.0
 *        Created:  18/06/20 17:12:20
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejandro Torres Guerra 
 *   Organization:  
 *
 * =====================================================================================
 */



#include <stdio.h>
#include <stdlib.h>


double rectangulo_area(double n1, double n2){
    return n1*n2;
}

double triangulo_area(double n1, double n2){
    return (n1*n2)/2;
}

double (*funct_dis[])(double n1, double n2) = {&rectangulo_area, &triangulo_area}; 

int main (int argc, char *argv[]){
    double b;
    double h;
    int opcion;
    double (*p)(double n1, double n2); 

    printf("Elige que quieres sacar el area:\n1 para el rectangulo\nCualquier numero para el triangulo\n");
    scanf("%i", &opcion);
    opcion--;

    printf("La base es: ");
    scanf("%lf",&b);

    printf("La altura es: ");
    scanf("%lf",&h);
  
    // if (opcion == 1){
    //     p = &rectangulo_area;
    //     printf("El area del rectangulo es %.2lf", (*p)(b,h));
    //     printf("\n");
    // }
    // else{
    //     p = &triangulo_area;
    //     printf("El area del triangulo es %.2lf", (*p)(b,h));
    //     printf("\n");
    // }
    
    double area = (*funct_dis[opcion])(b,h);
    printf("El area del rectangulo es %.2lf\n", area);

    return EXIT_SUCCESS;
}

