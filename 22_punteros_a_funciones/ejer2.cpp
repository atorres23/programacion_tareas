/*
 * =====================================================================================
 *
 *       Filename:  ejer2.cpp
 *
 *    Description: Teniendo varias funciones matematicas, preguntale al usuario que funcion quiere usar que x y calcular el valor de la funcion en esa x. 
 *
 *        Version:  1.0
 *        Created:  19/06/20 11:31:36
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejandro Torres Guerra 
 *   Organization:  
 *
 * =====================================================================================
 */




#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double lineal(double n1, double n2, double n3, double n4){
	return (n2*n1)+n3;
}

double cuadratica(double n1, double n2, double n3, double n4){
	return (n2*(pow(n1, 2)))+(n3*n1)+n4;
}


double exponencial(double n1, double n2,double n3, double n4){
	return pow(n2, n1);
}

double (*p)(double n1, double n2, double n3, double n4); 

int main (int argc, char *argv[]){
double x;
double a;
double b;
double c;
int opcion;

printf("Elige que quieres sacar el area:\n1.Lineal\n2.Cuadratica\n3.Exponencial\n");
scanf("%i", &opcion);

switch (opcion) {

case 1:
	printf("El numero x es: ");
	scanf("%lf",&x);

	printf("El numero a es: ");
	scanf("%lf",&a);
	
	printf("El numero b es: ");
	scanf("%lf",&b);
	
	p=lineal;
	printf("La formula de la funcion lineal es f(%.2lf)=%.2lf*%.2lf+%.2lf\n", x,a,x,b);
	printf("La funcion lineal es %.2lf", p(x,a,b,c));
	
break;

case 2:
	printf("El numero x es: ");
	scanf("%lf",&x);

	printf("El numero a es: ");
	scanf("%lf",&a);
	
	printf("El numero b es: ");
	scanf("%lf",&b);
	
	printf("El numero c es: ");
	scanf("%lf",&c);
	
	p=cuadratica;
	printf("La formula de la funcion cuadratica es f(%.2lf)=%.2lf*%.2lf^2 + %.2lf*%.2lf + %.2lf\n", x,a,x,b,x,c);
	printf("La funcion cuadratica es f(x)= %.2lf", p(x,a,b,c));
	
break;

case 3:
	printf("El numero x es: ");
	scanf("%lf",&x);

	printf("El numero a es: ");
	scanf("%lf",&a);
	
	p=exponencial;
	printf("La formula de la funcion exponencial es f(%.2lf)=%.2lf^%.2lf\n", x,a,x);
	printf("La funcion cuadratica es f(x)= %.2lf", p(x,a,b,c));
	
break;
}


return EXIT_SUCCESS;
}
