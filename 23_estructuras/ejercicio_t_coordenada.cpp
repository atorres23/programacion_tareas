#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //libreria necesaria para ejecutar el usleep

#define MAX 0x30

struct TCoordenada {
	double x;
	double y;	
};

struct TCoordenada pregunta (const char *dato){
	struct TCoordenada datos_recogidos;
	
	printf ("%s: \n", dato);
    printf ("\n");
    printf ("%s (x) = ", dato);
    scanf ("%lf", &datos_recogidos.x);
    printf ("%s (y) = ", dato);
    scanf ("%lf", &datos_recogidos.y);
	printf("\n");
	
	return datos_recogidos;
	
}

void mostrar_pantalla (const char *etiqueta, struct TCoordenada vector) {
	printf ("%s: (%.2lf, %.2lf)\n", etiqueta, vector.x, vector.y);
}

int main (int argc, char *argv[]) {
	
	printf ("POSICION Y VELOCIDAD\n\n");
    printf ("Recuerda este programa correra hasta que una de las posiciones (x o y) llegue a un numero mayor que 100 o 100. \n\n");


    struct TCoordenada posicion, velocidad;
   
    posicion = pregunta ("Posicion");
    velocidad = pregunta ("Velocidad");

    while (posicion.x < 100 && posicion.y < 100) { 
        posicion.x += velocidad.x;
        posicion.y += velocidad.y;
        system ("cls"); //cls porque lo hago en terminal de windows
        mostrar_pantalla ("Posicion inicial", posicion);
        mostrar_pantalla ("Velocidad a la que recorrera la posicion x e y respectivamente", velocidad);
        usleep (100000); //tiempo en microsegundos
    }

    return EXIT_SUCCESS;
}

