#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int d, y;
    int rv;
      
    printf ("dd/mm/yyyy: ");   
    rv = scanf (" %i/%*i/%i", &d, &y);
    /* Con "*" sobre la segunda i, indica que lee el mes pero no lo asigna pporque no queremos asignarlo*/              
    printf("rv: %i\n", rv);

    return EXIT_SUCCESS;
}
