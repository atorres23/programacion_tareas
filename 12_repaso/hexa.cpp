#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

int main (int argc, char *argv[]) {

    char leido[MAX];
    int numero;

    printf ("Hexa: ");
    scanf (" %[0-9a-fA-F]", /*&*/leido);
    /*Se quita el ampersan si definimos la variable leido ya que entonces el puntero apuntara a la direccion de memoria correspondiente en vez de &leido */   
    numero = atoi (leido); /*ASCII to integer = atoi, y convierte los ascii en numeros enteros*/

    /*Mi atoi particular*/
    int resultado = 0;
    for (int i=0; leido[i]!='\0'; i++)
        resultado = resultado * 10 + leido[i] - '0';
    printf ("Numero:\t%i\nResultado:\t%i\n", numero, resultado);
    printf ("\n");

    return EXIT_SUCCESS;
}
