
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int *p;

    p = (int *) malloc (sizeof (int));
       
    *p = 2;

    printf ("%i\n", *p);

    free(p);

    /*SIEMPRE QUE SE USE MALLOC HAY QUE PONER EL FREE PARA LIBERAR LA MEMORIA PORQUE SI NO NOS QUEDAMOS SIN ESPACIO*/

    return EXIT_SUCCESS;
}
