/*Usando un while y un scanf lean numeros enteros y los guarde en una variable hasta que el usuario pulse intro sin haber metido ningun numero*/

#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100
int main (int argc, char *argv[]) {

    int entrada[MAX];   
    int i = 0;
    bool noparar = true; /*cambiar porque esto es c++*/

    while (noparar && i<MAX){
        scanf (" i", &entrada[i++]);
    }    
    
    
    return EXIT_SUCCESS;
}
