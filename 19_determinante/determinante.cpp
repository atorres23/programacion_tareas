#include <stdio.h>
#include <stdlib.h>

#define N 3

void recoge_datos (double d[N][N]) { /*funcion llamada recoge datos, al ser void no tiene return, le paso un double de un array bidimensional de maximo N que es 3 */
	for (int fila=0;fila<N; fila++) /*para cuando fila es 0, sea menor que el valor de N que es 3 y se incrementa en 1 la variable fila*/
		for (int columna=0; columna<N; columna++) { /*dentro de la anterior condicion, se hace otra condicion donde sea lo mismo pero en vez de la fila, las columnas*/
			printf ("Elemento (%i, %i): ", fila+1, columna+1); /*Imprime por pantalla lo que quieres meter en la primera celda */
			scanf ("%lf", &d[fila][columna]); /*el usuario mete */
		} 
}

void salida (double d[N][N]) { /*funcion llamada salida, le paso un double de array bidimensional de maximo valor de N*/
	printf ("\n\n"); /*salto de linea*/
	for (int fila=0; fila<N; fila++){ /*bucle for donde tendra como condicion que el valor de fila es igual a 0, menor que N y se incrementa en 1 su valor*/
		for (int columna=0; columna<N; columna++) /*dentro del bucle for, se a�ade otro donde cumpla la condicion de que el valor de columna incial sea 0, sea menor que 3 y que se incremente en 1 mientras se haga el bucle*/
			printf ("%6.2lf", d[fila][columna]); /*se imprime por pantalla un numero de 2 decimales como mucho y 6 de longitud total contando enteros y decimales, es de tipo long float refieriendose al tipo de dato de double, el valor de d[valor de fila][valor de columna]*/
		printf ("\n");
	}
	printf ("\n");
}

int derecha (int columna) {
	if (columna < 0) /*si la el valor de columna es menor que 0*/
		return columna + N; /*se devuelve el valor de columna + el valor de n que es 3 definido al principio*/
	return columna; /*devuelve el valor de columna a la funcion derecha*/
}


int main (int argc, char *argv[]) {

	double matriz[N][N]; /*array bidimensional*/
	double determinante = 0;/*el valor de determinante es 0*/
	double producto; /*declaramos la variable producto como un double*/
	
	recoge_datos (matriz); /*llamada a funcion recoge datos*/
	
	for (int offset=0; offset<N; offset++) { /*bucle for que para que se haga es necesario que el offset sea 0, que sea menor que n que es 3 y se incrementa 1 el offset*/
		producto = 1; 	/*se pone valor a producto en 1*/
		for (int numero=0; numero<N; numero++) /*dentro del bucle for, se hace otra condicion en el que numero es 0, tiene que ser menor que n e incrementarse en 1*/
			producto *= matriz[numero][(numero+offset) % N]; /*el valor de prodcuto es igual al producto por la matriz[valor numero][resto de resultado de valor de numero mas offset entre N que es 3]*/
 		determinante += producto; /*determinante es igual a determinante mas el valor de producto*/
	}
	
	salida (matriz); /*se llama a la funcion salida con valor de matriz que es el array bidimensional*/
	printf ("Determinantes = %.2lf\n\n", determinante); /*imprimimos por pantalla que determinantes es igual al dato de %2lf que es el tipo de dato de double, y que tome el valor de determinante*/
	printf ("\n");
		
	
    return EXIT_SUCCESS;
}
