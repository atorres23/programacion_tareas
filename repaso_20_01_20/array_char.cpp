#include <stdio.h>
#include <stdlib.h>

#define MAX 20

/*Declarar dentro del array 20 unisigned char*/
/*Rellena con un for cada celda con su indice, como lo contaria un usuario empezando por l */
/*Cuadrados de los numeros*/
int main (int argc, char *argv[]) {
   
    unsigned char l[MAX];
    
    for (int i=0; i<MAX; i++)
        l[i]= (i + 1)*(i + 1);
      
    for (int i=0; i<MAX; i++)
        printf ("%i |", l[i]);
        printf ("\n");

    return EXIT_SUCCESS;
}
