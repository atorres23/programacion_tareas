#include <stdio.h>
#include <stdlib.h>
/* 1)Declaramos un array de caracteres que se llama frase, le preguntamos al ususuario una palabra y la vamos a leer con ese array de caracteres
   2) lista que tenga dos palabras hola y buenas noches
   3) Imprimir en la funcion main la cantidad de bytes que ocupa lista
   4) Calcular e imprimir la cantidad de celdas que tiene lista
   5) Sabiendo la cantidad de bytes que ocupa lista y lo que ocupa cada celd      a, imprimir todas las palabras que tenga, celda a celda.
*/

int main (int argc, char *argv[]) {

const char *lista[]={"Hola", "Buenas noches"};
   
  char palabra[20];

    printf ("Introduce una palabra:\n");
    scanf ("%s", palabra);

    printf ("La palabra introducida es %s\n", palabra);

    printf ("El tamaño en bytes de lista es %lu\n", sizeof(lista));

    long unsigned tamano = sizeof(lista)/sizeof(char *);

    printf ("EL numero de celdas de la lista es %lu\n", tamano);

    for (int contador = 0; contador < tamano; contador++)
        printf ("Los valores del array son: %s\n", lista[contador]);

    return EXIT_SUCCESS;
}
