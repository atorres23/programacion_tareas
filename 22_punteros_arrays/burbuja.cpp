#include <stdio.h>
#include <stdlib.h>

#define N 6

void muestra (int a[N]) {  //tipo de retorno nulo donde el nombre del modulo es muestra, donde hay un entero a de dimension N, es decir como esta definido como 6.
    for (int i=0; i<N; i++) //para que el entero i sea igual a 0, debera ser menor que N y se incrementara en 1
        printf ("%i", a[i]); //se imprime por pantalla el entero y el dato de salida es el a de dimernsion i que dependera del for.
    printf ("\n"); //salto de linea
}

int main (int argc, char *argv[]) {
    int A[N] = {2, 5, 3, 7, 4, 1}; //entero a de dimernsion N que es igual a 6 tendra como valores que estan dentro de las llaves
    muestra (A); //la funcion muestra tendra bucles

    for (int p=0; p<N-1; p++) //el bucle for se hara cuando p sea 0, menos que N-1 y se incrementara en uno cada vez que se haga el bucle
        for (int i=0; i<N-1; i++) //dentro de ese bucle habrá otro que el entero sea que i sea 0, memenor que N-1 y se incrementara en uno cada vuelta
            if (A[i] < A[i+1]) { //otro bucle if que si a de dimension entero i es menor que A de dimension i+1
                int aux = A[i+1]; // se define un entero llamado aux que es A de dimension i +1
                A[i+1] = A[i]; 
                A[i] = aux;//como A de dimension i es igual que A de dimension i+1, entonces la variable aux es A de dimension i
            }
    muestra (A); //cierre de llamada muestra

    return EXIT_SUCCESS;
}
