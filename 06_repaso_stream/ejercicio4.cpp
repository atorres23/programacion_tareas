#include <stdio.h>
#include <stdlib.h>

//const char *carta = "🂡";

//Con el const no me deja cambiar la variable carta para que no se haga una cadena mas larga y se cargue el programa//

//Siempre que ponga una variable tipo char de varios caracteres tendre que poner * antes de la variable//
int main (int argc, char *argv[]) {
    char carta[] = "🂡";
    //se crea un array en vez de dejar *carta porque viola el segmento.
    
    //char final = carta + 3;
    char *fin;
    fin = carta + 3;
    ++(*fin); //con *carta++ no se puede solo hacer eso porque si no avanzamos a la ultima direccion pero no sabemos como volver por eso se pone un *finpara que podamos saber volver al imprimir carta//
    
    //*carta++; //con *carta++ suma 1 la primera direccion de memoria, incrementa lo que tiene carta y con carta++ pasa a la siguiente direccion de la cadena de caracter por ejemplo cola seria ola solo. 
    
    //Se puede usar carta[3] por ejemplo para ir a 3 direcciones de memoria y no hace falta tener otra variable para volver.(*fin)
    
    //Otra forma de hacerlo es ++(*(carta + 3)); se llama connotacion de puntero que es poner todo en una linea.
    
    //El segundo paso se pone char quitando el cons de arriba y se pone abajo de int main como si fuera variable local//
    printf ("%s\n", carta);

    //suministra direccion de memoria con %s byte a byte//

    return EXIT_SUCCESS;
}
