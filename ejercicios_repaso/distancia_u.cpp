#include <stdio.h>
#include <stdlib.h>


/*A que distancia desde el principio y direccion de memoria de la primera u
*/

int main (int argc, char *argv[]) {
  
   char frase[] = "El sol esta muy alto en el cielo"; 
  int i;

   for (i=0; frase[i]!='u' && frase[i] != '\0'; i++);

   if (frase[i] == 'u') {
   printf ("La posicion de la u es: %i.\n"
             "En la direccion: %p\n", i, &frase[i]); 
   
   }
   return EXIT_SUCCESS;
}
