
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    int b = 1; 
    b=++b + 5; /*La diferencia de poner ++b o b++ es que cuando pones b++ se coge el valor de b se incrementa pero solo se pone pero se sigue sumando el valor inicial de b asique no vale para nada poner b++ porque es igual a b + 5 porque sale 6, lo unico que lo ponemos porque asi tambien funciona. El b++ se mira el valor de b y se incrementa pero no se suma solo se incrementa sobre la memoria pero en este caso sigue valiendo 1.
    En este caso de ++b incrementa b y luego mira lo que vale para sumarselo a 5 y sera 7*/
    printf ("b=%i\n", b);

    return EXIT_SUCCESS;        
}

