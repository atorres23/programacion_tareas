#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    int num1, num2;
    char opcion;
    

    printf ("Introduce el primer numero:\n");
    scanf (" %i", &num1);
 
    printf("Elige el segundo numero:\n");    
    scanf (" %i", &num2);
  
    printf("Quieres sumar o restar:\n");
    scanf (" %c", &opcion);
  
    if ( opcion == 's')
     printf(" %i + %i = %2i\n", num1, num2, num1 + num2);
    else 
    if( opcion == 'r')
     printf( "%i - %i = %2i\n", num1, num2, num1 - num2);    
   
     return EXIT_SUCCESS;
}
