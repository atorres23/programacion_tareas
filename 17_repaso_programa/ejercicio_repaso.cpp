#include <stdio.h>
#include <stdlib.h>

//Programa que pida un numero
//Numeros que dividan entre 0
int main (int argc, char *argv[]) {

    unsigned numero;
    
    
    printf("¿Que numero quieres poner?\n");
    scanf("\n%u", &numero);

    for (unsigned d=numero/2; d>0; d--)
        if (! (numero % d))
            printf ("%u ", d);
    printf ("\n");

    return EXIT_SUCCESS;
}
