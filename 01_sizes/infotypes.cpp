#include<stdio.h>


int main () {
    long int var_longint;
    
    printf ("longint: %lu bytes.\n", sizeof (var_longint));
    printf ("slongint: %lu bytes.\n", sizeof (signed long int));
    printf ("sshortint: %lu bytes.\n", sizeof (signed short int));
    printf ("ulongint: %lu bytes.\n\n", sizeof (unsigned long int));
    printf ("char: %lu bytes.\n\n", sizeof (char));
    printf ("sinedgint: %lu bytes.\n", sizeof (signed int));
    printf ("unsignedint: %lu bytes.\n", sizeof (unsigned int));
    printf ("schar: %lu bytes.\n", sizeof (signed char));
    printf ("uchar: %lu bytes.\n", sizeof (unsigned char));
    return 0;
}
