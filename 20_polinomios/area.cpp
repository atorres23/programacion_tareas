#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#define MAX_ERROR .001

double f (double *polinomio,  double grado, double x) {  //tipo de dato doble llamado f, (puntero a variable polinomio, variable de tipo dato double llamado grado y variable igual llamada x) 
	
	double altura = 0,	// tipo de dato doble de variable altura, inicializada con valor 0
		   potencia = 1; //tambien tipo de dato doble de variable llamada potencia, inicializada con valor 1
		   
	for (int p=0; p<grado; p++, potencia*=x) //condicion que para que se efectue, el entero p sea igual a 0, que sea menor que el valor de grado y que la variable potencia sea igual a la potencia misma por la variable x
		altura += polinomio[p] * potencia;  // variable llamada altura que es igual a la altura misma mas el producto de polinomio de la celda que sea la variable p por la variable de potencia
		
	return altura; //retorna en valor de altura
}


double *pedir (int *dim) { //puntero a variable pedir de tipo doble, puntero a variable dim de tipo entero
	double buffer, *pol = NULL; // dato de tipo doble llamado buffer e inicializado con valor de NULL en puntero a variable pol
	char end[2]; //caracter de variable llamado end de indice de array 2, es decir la celda 2
	
	*dim = 0; //inicializamos puntero de dim a 0
	printf ("Introduce el polinomio que quieres sacar:\n"); 
	printf ("ej: 6^3 + 5x + 4.3 -> (2.3 1 6)\tPolinomio: "); 
	
	scanf (" %*[(]"); //estos dos scanf quieren decir que los valores que ponga el usuario tienen que estar entre parentesis
	 
	do { //condicion de hacer, mientras...
		pol = (double *) realloc(pol, (*dim+1) * sizeof(double)); //??
		scanf(" %lf", &buffer); //pedir datos a usuario, de direccion de memoria de buffer donde se guardara ese valor
		pol[(*dim)++] = buffer; // pol el puntero de dim se incrementara en 1 sera igual a buffer
	}while  (!scanf(" %1[)]", end)); //mientras el dato de la introduccion del usuario sea diferente a que el primer digito del usuario sea un cierre de parentesis (?)
	
	return pol; //retorna en valor de pol
	}
	

void mostrar (double *p, int d) { //funcion llamada mostrar (de tipo de dato doble puntero a p, entero de variable llamada d)
	for (int i=d-1; i>0; i--) //condicion para que el entero i sea igual a variable d-1, que i sea mayor que 0, y que se decremente i en 1 cada vez que se haga el bucle
		printf ("%.2lfx^(%i) + ",p[i], i); //imprimir un numero long float de 2 decimales como maximo de variable p en indice de array i (entero de i)
	printf ("%.2lf\n", p[0]); // imprimir de 2 decimales como maximo, del valor p de la celda 0
}

double integral (double *p, int d, int li, int ls, double inc) { 	//tipo de dato doble llamado integral (double de puntero de p, entero de d, entero de li, entero de ls, double de inc)
	double suma = 0; //tipo de dato doble de varible donde su valor es 0
	for (double x=li; x<ls; x+=inc) //condicion para que se haga el bucle, tipo de dato doble de variable llamada x, tendra que ser menor que ls y tendra el valor de x misma con el valor de la variable inc
		suma += f(p, d, x); // ??
	suma *= inc; // suma sera igual al producto de suma por la variable inc
	
	return suma; // retorno de valor de suma
}


int main (int argc, char *argv[]) {
	double *pol, l_inferior, l_superior; //tipo de dato doble de puntero a pol, de variable llamada l_inferior y l_superior
	int dim; 	//variable de tipo entero llamado dim
	char opt;	//variable de tipo caracter llamado opt
	int otro_pol;	//variable de tipo entero llamado otro_pol
	int otra_area;	//variable de tipo entero llamado otro_area
	double area, ultima_area; 	//variable de tipo dato doble llamado area y ultima_area
	
	do {	//hacer
		pol = pedir (&dim); //pol es igual que el valor pedir de valor donde se guarda el valor de dim
		
		do {	//hacer
			printf ("\n");
			printf ("Limite inferior: "); 
			scanf ("%lf", &l_inferior); //pedir dato de tipo long float y se guarda el valor en la direccion de l_inferior
			printf ("Limite superior: ");
			scanf ("%lf", &l_superior); //pedir dato de tipo long float y se guarda el valor en la direccion de l_superior
			
			if (l_inferior > l_superior) { //condicion si l_inferior es menor que l_superior
				double aux = l_inferior; 	// tipo de datos doble llamado aux para que sea igual su valor de l_inferior
				l_inferior = l_superior;	// valor de l_inferior es igual a l_superior
				l_superior = aux;	// valor l_superior es igual a aux
			}	
			
			double inc = fabs (l_superior - l_inferior); //variable de tipo dato double llamado  inc que tendra el valor de fabs que sera el resultado de restar l_superior con l_inferior
			 
			area = integral (pol, dim , l_inferior, l_superior, inc); //el valor de area es igual a la funcion de integra y el valor de pol, dim, l_inferior, l_superior e inc
			ultima_area = area - 5 * MAX_ERROR;	//el valor de ultima_area es igual a la resta de area con 5 por el maximo error que es .001
			
			for (inc/=2; fabs (area - ultima_area) > MAX_ERROR; inc/=2){ //condicion que para inc sea igual a la division entre inc y 2, fabs y su valor del resultado de la resta de area con ultima_area sea menor que MAX_ERROR y que inc sea igual a inc misma entre 2
				ultima_area = area; //el valor de ultima_area es igual al valor de area
				area = integral (pol, dim, l_inferior, l_superior, inc); // el valor de area es igual a la funcion integral y el valor de pol, dim, l_inferior, l_superior e inc
			}
			
			printf ("El area de: \n");
			mostrar (pol, dim); //funcion mostrar de valor pol y dim
			printf ("entre [%.2lf, %.2lf] es: \n", l_inferior ,l_superior);
			printf ("\nArea = %.3lf\n", area);
			printf ("\n");
			printf ("\n");
			printf ("Quiere calcular otra area (s/n): ");
			scanf (" %c", &opt); 
			otra_area = tolower(opt) != 'n'; // el valor de otra_area es igual a tolower de valor opt sea diferente al caracter n
		} while (otra_area);	//mientras valor otra_area
				
			printf ("Quiere trabajar en otro polinomio (s/n): ");
			scanf (" %c", &opt);
			otro_pol = tolower(opt) != 'n'; //el valor de otro_pol  es igual a tolower de valor opt sea diferente al caracter n
		
	} while (otro_pol);
		
		printf ("\n");
		printf ("\n");
		return EXIT_SUCCESS;
}

