#include <stdio.h>
#include <stdlib.h>

#define DIM 3
/*Declaracion de variables constantes*/
const char *const bases[] = {
    "cartesianas",
    "cilindricas",
    "esferícas",
    NULL /*(void *) 0 */
};

/*Declaracion de variables constantes, primer corchete fila, segundo corchete columna, empezando por 0,1,2. Si es [1][2] se refiere a la z de la 2º fila*/
/*El segundo corchete siempre tiene que tener un valor, el primero lo podemos dejar vacio*/
const char * const componentes [][DIM] = {
    /*0*/ /*1*//*2*/
    {"X", "Y", "Z"}, /*0*/
    {"Ro", "Theta", "Z"},/*1*/
    {"R", "Theta", "Phi"} /*2*/
};    
enum TBase {cart, cil, esf, BASES}; /*Base 0 cart, base 1 cil, base 2 esf*/
/*enum declara las constantes, esta linea hace que solo podamos poner un 0,1,2,3 si ponemos un 5 no hace nada*/

void title () {
    system ("clear");
    system ("toilet -fpagga --metal VEC3D");
    printf ("\n\n");
}

void print_options (const char * const estadio) {
    const char **base = (const char **) bases;

    printf ("Elige base %s:\n", estadio);
    printf ("\n");
    printf ("\n");
    for (int i=0; *base!=NULL; base++, i++)
        printf ("\t%i.- %s", i+1, *base);
    
    printf ("\n");
    printf ("\n");
}

enum TBase ask_option () {
    unsigned eleccion;

    do {
    printf ("                                          \r");
    printf ("   Opcion: ");
    scanf (" %u", &eleccion);
    printf ("\x1B[1A");

} while (eleccion <= 0 || eleccion > BASES);

 printf ("\n");
 eleccion--;

 return (enum TBase) eleccion;
}
void menu (enum TBase *src, enum TBase *dst){
    title ();
    print_options ("inicial"); /* es un string*/
    *src = ask_option ();
    print_options ("destino");
    *dst = ask_option ();
}

void ask_vector (enum TBase base, double vector[DIM]) {
    printf ("Componente: ");
    printf ("\n");
    printf ("\n");

    for (int c=0; c<DIM; c++){
        printf (" %s: ", componentes[base][c]);
        scanf (" %lf", &vector[c]);
    }
}

int main (int argc, char *argv[]) {

    enum TBase srcbas, dstbas; /*dos variables, origen y destino, que tipo de origen quiero y que destino o tipo quiero si cilindricas, esfericas...*/
    double src_vec[DIM], dst_vec[DIM]; /*dos vectores que pueden tener 3 double, porque DIM esta definido como 3*/
    menu ( &srcbas, &dstbas );  /*Pregunta que quiere, base origen y base deetino.*/ /*Siempre que veamos en menu un & quiere decir paso x referencia*/
    /*Paso por referencia de pasar, cambiar el puntero a otra direccion de memoria, sin embargo, *src es escribir dentro de la variable srcbas en vez de cambiar la direccion de memoria*/
    title ();
    ask_vector (srcbas, src_vec);

    if (srcbas == dstbas)

    return EXIT_SUCCESS;
}
