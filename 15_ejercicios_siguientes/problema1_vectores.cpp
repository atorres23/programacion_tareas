#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    double buffer;
    double *vec = NULL;
    static int dim = 0;
    char end;

    printf("ej: (1.5 2 3.7).\tVector: ");
    scanf(" %*[(]");
    do{
        vec = (double *) realloc(vec, (dim+1) * sizeof(double)); /*el dim son las celdas que tenemos, si es sobre 2 dimensiones seran 2 numeros por ejemplo (3,7)*/
        scanf(" %lf", &buffer);
        vec[dim++] = buffer;

    }while (!scanf(" %1[)]", &end)); /*si no te devuelve un 1 date una vuelta, 0 es falso, no falso es verdadero, entonces hasta que no encuentre el parentesis se da otra vuelta; mientras no lea y no asigne*/

    printf("\n\t( ");
    for (int componente=0; componente<dim; componente++)
     printf("%6.2lf", vec[componente]); /*6 caracteres y 2 para parte decimal, el punto tambien cuenta como un caracter, entonces sera para 3 para parte entera, 1 para el punto y 2 para decimal= 3+1+2=6*/
    printf(" )\n");

    free(vec);   
    return EXIT_SUCCESS;
}
