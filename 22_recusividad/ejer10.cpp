/*
 * =====================================================================================
 *
 *       Filename:  ejer10.cpp
 *
 *    Description: Calcula la funcion e de manera recursiva hasta que la diferencia entre dos pasos sea menor que una constante definida por el usuario, E 
 *
 *        Version:  1.0
 *        Created:  13/06/20 14:31:30
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejandro Torres Guerra 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdlib.h>
#include <stdio.h>

int factorial(int n) {
    int a=1;
    if (n>=1){
        a=n*factorial(n-1);
    }
    return a;
}

double fact_e(int n){
    double error=.0001;
    double nuevo=1. / factorial(n);
    if (nuevo < error)
        return nuevo;
    return nuevo + fact_e(n+1);
}

int main (int argc, char *argv[]) {
    printf("Para el numero e desde el termino 0 = %.5lf\n", fact_e(0));
    return EXIT_SUCCESS;
}
