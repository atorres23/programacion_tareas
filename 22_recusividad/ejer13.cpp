/*
 * =====================================================================================
 *
 *       Filename:  ejer13.cpp
 *
 *    Description: Haz un programa que pregunte al usuario e imprima el ordinal correspondiente usando una funcion recursiva 
 *
 *        Version:  1.0
 *        Created:  14/06/20 16:12:42
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejandro Torres Guerra
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdlib.h>
#include <stdio.h>

const char *ordinal[3][10] = {
    { "", "primero", "segundo", "tercero", "cuarto", "quinto", "sexto", "séptimo", "octavo", "noveno" },
    { "", "décimo", "vigésimo", "trigésimo", "cuatrigésimo", "quincuagésimo", "sexagésimo", "septuagésimo", "octogésimo", "nonagésimo" },
    { "", "centésimo", "bicentésimo", "tricentésimo", "cuadrigentésimo", "quingentésimo", "sexgentésino", "septingentésimo", "octingentésimo", "noningentésimo" }
};


void numero_ordinal (int n, int f){
    if (f < 3){
        numero_ordinal (n/10, f+1);
        printf("%s", ordinal[f][n%10]);
    }
}

int main (int argc, char *argv[]) {
    int n;

    printf("Mete el numero en cifras: \n");
    scanf("%i",&n);

    printf("El numero que has introducido, en ordinal es el:\n");
    numero_ordinal(n,0);
    printf("\n");
   
    return EXIT_SUCCESS;
}
