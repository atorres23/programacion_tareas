/*
 * =====================================================================================
 *
 *       Filename:  ejer8.cpp
 *
 *    Description: ejercicio8, funcion no recursiva 
 *
 *        Version:  1.0
 *        Created:  09/06/20 18:18:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejandro Torres Guerra
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define N 29

int numero_de_divisores (int n) {
    int static divisor = 0;

    if (n<1)
        return divisor;

    if (N % n == 0)
        divisor++;
    return numero_de_divisores(n-1);
}

bool es_primo (int n) {
    bool primo = false;

    int divisor = numero_de_divisores(N);

    if (divisor == 2) //por 1 y por el mismo numero
        primo = true;
    return primo;
}

int main (int argc, char *argv[]) {
    int divisor = numero_de_divisores(N);
    bool primo = es_primo(N);

    if (primo == true)
        printf("%i es primo\n",N);
    else 
        printf ("%i no es primo y tiene %i divisores\n", N, divisor);

    return EXIT_SUCCESS;
}
