/*
 * =====================================================================================
 *
 *       Filename:  ejer4.cpp
 *
 *    Description: Ejercicio 4 rehacer el ejer3 para que los imprima en orden creciente
 *
 *        Version:  1.0
 *        Created:  06/06/20 17:25:50
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejandro Torres 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define N 9

void numeros_naturales (int n) {
    if (n<1)
        return;

    numeros_naturales(n-1);
    printf ("%i",n);
}


int main(int argc, char *argv[]) {

  numeros_naturales(N);
  
  return EXIT_SUCCESS;

}
