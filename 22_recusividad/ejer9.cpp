/*
 * =====================================================================================
 *
 *       Filename:  ejer9_ejemplo.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  12/06/20 17:08:37
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejandro Torres Guerra 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int factorial(int n){
    int a = 1;
    if (n >= 1){
        a = n * factorial(n-1);
    }
    return a;
}

double fact_e(int n){
    if(n == 0){
        return 1;
    }
    return 1. / factorial(n) + fact_e(n-1);
}

int main (int argc, char * argv[]) {

    int n;
  
    printf("Introduce un termino n para numero e:\n");
    scanf(" %i", &n);
    
    printf("%.4lf\n",fact_e(n));

    return EXIT_SUCCESS;
}
