/*
 * =====================================================================================
 *
 *       Filename:  ejer7.cpp
 *
 *    Description: Ejer 7 imprime los divisores de n 
 *
 *        Version:  1.0
 *        Created:  06/06/20 18:07:53
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejandro Torres 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define N 30 //numero del que vamos a sacar sus divisores

int num_div (int n){
    if(n<1)
        return 0;

    if (N % n == 0)
        printf ("%i - ", n);
    return num_div(n-1);
}

int main (int argc, char *argv[]){

    printf("Numeros que son divisores de %i: \n", N);
    num_div(N);

   return EXIT_SUCCESS;  

}
