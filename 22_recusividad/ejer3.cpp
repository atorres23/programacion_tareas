/*
 * =====================================================================================
 *
 *       Filename:  ejer3.cpp
 *
 *    Description: Ejercicio 3 imprimir numeros naurales desde n hasta 1 en orden decreciente.
 *
 *        Version:  1.0
 *        Created:  06/06/20 17:25:50
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alejandro Torres 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define N 9

void numeros_naturales (int n) {
    if (n<1)
        return;

    printf ("%i",n);
    numeros_naturales(n-1);
}


int main(int argc, char *argv[]) {

  numeros_naturales(N);
  
  return EXIT_SUCCESS;

}
