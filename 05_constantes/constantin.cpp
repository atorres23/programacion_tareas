#include <stdio.h>

#define EURO 166.386 /*Constante simbolica*/

int main () {
    double input;

    printf ("How much do you wanna change?");
    scanf (" %lf", &input); /*&input se lee la direccion de la variable input*/

    printf ("%.2lf₧  => %.2lf€", input, input / EURO); /*La expresion %.2lf es para que imprima dos caracteres decimales de long float. Si se pusiese 9.2, es para indicar que hay nueve caracteres con 2 decimales*/

    return 0;
}
